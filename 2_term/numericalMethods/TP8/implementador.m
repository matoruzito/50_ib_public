source("funciones.m");

N = 10000;
fil = 1;

# Carga la condicion inicial
u0 = [0, 1, 0, -1, 1, 0, -1, 0];

#Para calcular energía mecánica cambiar a esta condición inicial y descomentar
#u0 = [0, 0.5, 0, -0.5, 1, 0, -1, 0];

%No converge implicito antes de 228
for iter = 10000:1:N

    Tini = 0; # Tiempo inicial
    Tfin = 2 * pi; # Tiempo final
    n = iter; # Instantes de tiempo donde se evaluara la solucion
    contadorDeEjecuciones = 0;
    tiempoExec = 0;

    global MAX_ITERACIONES
    global MIN_TOLERANCIA

    MAX_ITERACIONES = 200;
    MIN_TOLERANCIA = 1e-6;

    # Evaluaremos en "n" puntos equiespaciados entre Tini y Tfin
    t = linspace(Tini, Tfin, n);

    resultadosExecVSn(fil, 1) = n;
    resultadosTiemposVSn(fil, 1) = n;
    errorVsN(fil, 1) = n;

    # Euler explícito
    resultadosEE = titaMethod("miMaquinaDerivadora", u0, t, 0);
    resultadosExecVSn(fil, 2) = contadorDeEjecuciones;
    resultadosTiemposVSn(fil, 2) = tiempoExec;
    errorVsN(fil, 2) = norm(resultadosEE(end, 5:8) - resultadosEE(1, 5:8));
    contadorDeEjecuciones = 0;
    tiempoExec = 0;
    # Euler implícito
    resultadosEI = titaMethod("miMaquinaDerivadora", u0, t, 1);
    resultadosExecVSn(fil, 3) = contadorDeEjecuciones;
    resultadosTiemposVSn(fil, 3) = tiempoExec;
    errorVsN(fil, 3) = norm(resultadosEI(end, 5:8) - resultadosEI(1, 5:8));
    contadorDeEjecuciones = 0;
    tiempoExec = 0;
    # Crank Nicholson
    resultadosCN = titaMethod("miMaquinaDerivadora", u0, t, 0.5);
    resultadosExecVSn(fil, 4) = contadorDeEjecuciones;
    resultadosTiemposVSn(fil, 4) = tiempoExec;
    errorVsN(fil, 4) = norm(resultadosCN(end, 5:8) - resultadosCN(1, 5:8));
    contadorDeEjecuciones = 0;
    tiempoExec = 0;
    # LSODE de Octave
    resultadosLSODE = executeLsode("miMaquinaDerivadora", u0, t);
    resultadosExecVSn(fil, 5) = contadorDeEjecuciones;
    resultadosTiemposVSn(fil, 5) = tiempoExec;
    errorVsN(fil, 5) = norm(resultadosLSODE(end, 5:8) - resultadosLSODE(1, 5:8));
    contadorDeEjecuciones = 0;
    tiempoExec = 0;

    fil = fil + 1;

endfor

% Energía mecánica en función del instante de tiempo.
G = 6.67e-11; %Cte de gravitación universal
M_1 = 4 / G;
M_2 = 4 / G;

energia(:, 1) = t;

for iter1 = 1:N

    #Energia = 1/2 * M * |v|^2 (se me va la raiz con el cuadrado) + Epotencial
    tempPotencialM = (G * M_1 * M_2) / (sqrt((resultadosEE(iter1, 7) - (resultadosEE(iter1, 5)))^2 + (resultadosEE(iter1, 8) - resultadosEE(iter1, 6))^2));

    tempCineticaM1 = 0.5 * M_1 * (resultadosEE(iter1, 1)^2 + resultadosEE(iter1, 2)^2);
    tempCineticaM2 = 0.5 * M_2 * (resultadosEE(iter1, 3)^2 + resultadosEE(iter1, 4)^2);
    energia(iter1, 2) = tempCineticaM1 + tempCineticaM2 - tempPotencialM;

    tempPotencialM = (G * M_1 * M_2) / (sqrt((resultadosEI(iter1, 7) - (resultadosEI(iter1, 5)))^2 + (resultadosEI(iter1, 8) - resultadosEI(iter1, 6))^2));
    tempCineticaM1 = 0.5 * M_1 * (resultadosEI(iter1, 1)^2 + resultadosEI(iter1, 2)^2);
    tempCineticaM2 = 0.5 * M_2 * (resultadosEI(iter1, 3)^2 + resultadosEI(iter1, 4)^2);
    energia(iter1, 3) = tempCineticaM1 + tempCineticaM2 - tempPotencialM;

    tempPotencialM = (G * M_1 * M_2) / (sqrt((resultadosCN(iter1, 7) - (resultadosCN(iter1, 5)))^2 + (resultadosCN(iter1, 8) - resultadosCN(iter1, 6))^2));
    tempCineticaM1 = 0.5 * M_1 * (resultadosCN(iter1, 1)^2 + resultadosCN(iter1, 2)^2);
    tempCineticaM2 = 0.5 * M_2 * (resultadosCN(iter1, 3)^2 + resultadosCN(iter1, 4)^2);
    energia(iter1, 4) = tempCineticaM1 + tempCineticaM2 - tempPotencialM;

    tempPotencialM = (G * M_1 * M_2) / (sqrt((resultadosLSODE(iter1, 7) - (resultadosLSODE(iter1, 5)))^2 + (resultadosLSODE(iter1, 8) - resultadosLSODE(iter1, 6))^2));
    tempCineticaM1 = 0.5 * M_1 * (resultadosLSODE(iter1, 1)^2 + resultadosLSODE(iter1, 2)^2);
    tempCineticaM2 = 0.5 * M_2 * (resultadosLSODE(iter1, 3)^2 + resultadosLSODE(iter1, 4)^2);
    energia(iter1, 5) = tempCineticaM1 + tempCineticaM2 - tempPotencialM;
endfor

% Guardamos los results en archivo. CUIDADO QUE SOBREESCRIBE!
save("-ascii", "data/A_EE.dat", "resultadosEE");
save("-ascii", "data/B_EI.dat", "resultadosEI");
save("-ascii", "data/C_CN.dat", "resultadosCN");
save("-ascii", "data/D_LSODE.dat", "resultadosLSODE");

% Numeros de llamados a la funcion para cada metodo
save("-ascii", "data/1_numExecVSn.dat", "resultadosExecVSn");

% Tiempo de execution para cada metodo
save("-ascii", "data/1_tiempoExecVSn.dat", "resultadosTiemposVSn");

% Error a una órbita para cada método
save("-ascii", "data/1_error1Orbita.dat", "errorVsN");

% Energia para cada instante de tiempo
save("-ascii", "data/1_conservEnergy.dat", "energia");
