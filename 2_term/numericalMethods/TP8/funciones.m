% Instituto Balseiro 2023
% @ Matías Serino Marin

global MAX_ITERACIONES
global MIN_TOLERANCIA
global contadorDeEjecuciones
global tiempoExec

% m = r1(t) | n = d(m) / dt
% o = r2(t) | p = d(o) / dt
% m = (x1(t), y1(t)) función vectorial
% o = (x2(t), y2(t)) función vectorial

%x1 = componenteX(r1(0)) = m(0)  >>> Componente x de la función m evaluada en cero
%x2 = componenteX(r2(0)) = o(0)  >>> Componente x de la función o evaluada en cero
%y1 = componenteY(r1(0) = m(0))  >>> Componente y de la función m evaluada en cero
%y2 = componenteY(r2(0) = o(0))  >>> Componente y de la función o evaluada en cero
% Interpretación física: n , p son velocidades.
% Recibo un vector x = [nx, ny, px, py, x1, y1, x2, y2]
% Recibo un vector x = [1,  2,  3,  4,  5,  6,  7,  8 ]

function xpunto = miMaquinaDerivadora(x, t)
    global contadorDeEjecuciones;
    G = 6.67e-11; %Cte de gravitación universal
    M_1 = 4 / G;
    M_2 = 4 / G;

    d = sqrt((x(7) - (x(5)))^2 + (x(8) - x(6))^2); % |r2 - r1| Distancia
    d3 = d * d * d; % Distancia al cubo

    xpunto(1) = ((G * M_2) / d3) * (x(7) - x(5)); % Componente X de la aceleración de r1
    xpunto(2) = ((G * M_2) / d3) * (x(8) - x(6)); % Componente Y de la aceleración de r1
    xpunto(3) = ((G * M_1) / d3) * (x(5) - x(7)); % Componente X de la aceleración de r2
    xpunto(4) = ((G * M_1) / d3) * (x(6) - x(8)); % Componente Y de la aceleración de r2
    xpunto(5:8) = x(1:4); %Velocidades (Derivadas de R1 y R2)
    contadorDeEjecuciones = contadorDeEjecuciones + 1;
endfunction

function resultados = titaMethod(func, u0, t, tita)
    global MIN_TOLERANCIA;
    global MAX_ITERACIONES;
    global tiempoExec;

    initialTime = time();

    n = size(t)(2); # Cantidad de puntos en los que se requiere la solucion
    m = size(u0)(2); # Cantidad de funciones incognita
    resultados = zeros(n, m); # matriz solucion
    resultados(1, :) = u0; # Primer punto: condicion inicial

    for i = 2:n
        resultados(i, :) = resultados(i - 1, :); %Semillamos con el punto anterior.
        # Evaluamos la funcion func, pasandole como argumentos el punto anterior
        delta1 = feval(func, resultados(i - 1, :)', t(i - 1));

        # Calculamos el paso de tiempo (Tactual-Tanterior)
        h = t(i) - t(i - 1);

        errorIteracion = 2 * MIN_TOLERANCIA;
        iteraciones = 0;

        while (iteraciones < MAX_ITERACIONES && errorIteracion > MIN_TOLERANCIA)
            delta2 = feval(func, resultados(i, :)', t(i));

            temporalTita = resultados(i - 1, :) + (h * (tita * delta2 + (1 - tita) * delta1));

            errorIteracion = norm(temporalTita - resultados(i, :));

            resultados(i, :) = temporalTita;
            iteraciones = iteraciones + 1;
        endwhile

        if (iteraciones == MAX_ITERACIONES)
            error("ERROR: No convergió el asunto");
        endif

    endfor

    tiempoExec = time() - initialTime;
endfunction

function resultadosLSODE = executeLsode(func, u0, t)
    global MIN_TOLERANCIA;
    global MAX_ITERACIONES;
    global tiempoExec;

    initialTime = time();
    n = size(t)(2); # Cantidad de puntos en los que se requiere la solucion
    m = size(u0)(2); # Cantidad de funciones incognita

    lsode_options("integration method", "adams");
    [resultadosLSODE, status, messagge] = lsode(func, u0, t);

    tiempoExec = time() - initialTime;

    if (status != 2)
        error(message);
    endif

endfunction
