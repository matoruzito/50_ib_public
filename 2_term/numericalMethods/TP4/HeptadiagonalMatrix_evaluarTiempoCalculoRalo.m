% @Matias Serino Marin
% Instituto Balseiro - 2022

% Evaluar el tiempo de cálculo para dierentes tamaños de la matriz del sistema
% Determinar dependencia en N hasta aprox n = 20
% CASO UTILIZANDO ALMACENAMIENTO RALO
% Datos iniciales

tiempos = [];
enes = [];

for n = 3:1:150

    N = 8 * (n * n * n);

    semilla = zeros(n);
    tolerancia = 10e-6;
    maximumIterations = N;

    SD = sparse(1:N, 1:N, 6 * ones(N, 1), N, N);
    if1 = setdiff(1:N - 1, n:n:n * (8 * n^2 - 1));
    SU1 = sparse(if1, if1 + 1, -1 * ones(1, (n - 1) * 8 * n^2), N, N);
    [i, j] = find([ones(n * (2 * n - 1), 4 * n); zeros(n, 4 * n)]);
    if2 = i + (j - 1) * 2 * n^2;
    SU2 = sparse(if2, if2 + n, -1 * ones(1, n * (2 * n - 1) * 4 * n), N, N);
    if3 = 1:2 * n^2 * (4 * n - 1);
    SU3 = sparse(if3, if3 + 2 * n^2, -1 * ones(1, 2 * n^2 * (4 * n - 1)), N, N);
    A = SD + SU1 + SU1' + SU2 + SU2' + SU3 + SU3'; % Almacenamiento "ralo"

    %A = full(SD + SU1 + SU1' + SU2 + SU2' + SU3 + SU3'); % Almacenamiento "lleno"
    b = ones(N, 1) / (n + 1)^2;

    t = time(); %Comienzo a contar el tiempo de ejecución
    [x, condicionDeConvergenciaUsada] = pcg(A, b, tolerancia, maximumIterations); %Aplicamos gradientes conjugados sin ponerle champú, digo, acondicionador
    tiempos = [tiempos; time() - t];
    enes = [enes; n];

    %CondicionDeConvergenciaUsada muestra 0 si fue por tolerancia y 1 si fue por maxIterations
    printf('%d, %f, %f \n', n, tiempos(end), condicionDeConvergenciaUsada);
endfor
