% @Matias Serino Marin
% Instituto Balseiro - 2022

% Evaluar el tiempo sin crema de enjuague
% Se utilizan matrices ralas en todos los casos
% Datos iniciales

tiempos = [];
enes = [];

for n = 3:1:100

    N = 8 * (n * n * n);

    semilla = zeros(n);
    tolerancia = 10e-6;
    maximumIterations = N;

    SD = sparse(1:N, 1:N, 6 * ones(N, 1), N, N);
    if1 = setdiff(1:N - 1, n:n:n * (8 * n^2 - 1));
    SU1 = sparse(if1, if1 + 1, -1 * ones(1, (n - 1) * 8 * n^2), N, N);
    [i, j] = find([ones(n * (2 * n - 1), 4 * n); zeros(n, 4 * n)]);
    if2 = i + (j - 1) * 2 * n^2;
    SU2 = sparse(if2, if2 + n, -1 * ones(1, n * (2 * n - 1) * 4 * n), N, N);
    if3 = 1:2 * n^2 * (4 * n - 1);
    SU3 = sparse(if3, if3 + 2 * n^2, -1 * ones(1, 2 * n^2 * (4 * n - 1)), N, N);

    A = SD + SU1 + SU1' + SU2 + SU2' + SU3 + SU3'; % Almacenamiento "ralo"
    b = ones(N, 1) / (n + 1)^2;

    t = time(); %Comienzo a contar el tiempo de ejecución
    [x, condicionDeConvergenciaUsada, relres, cantIteracionesRealizadas] = pcg(A, b, tolerancia, maximumIterations); %Aplicamos gradientes conjugados sin ponerle champú, digo, acondicionador
    tiempos = [tiempos; time() - t];
    enes = [enes; n];

    printf('%d %f %d %d\n', n, tiempos(end), condicionDeConvergenciaUsada, cantIteracionesRealizadas);
endfor
