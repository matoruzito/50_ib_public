% @Matias Serino Marin
% Instituto Balseiro - 2022

%Elementos utilizados para evaluar el tiempo de cálculo.
tiempos = [];
enes = [];

for n = 1:300

    % SEL: A*x = b generado aleatoriamente.
    A = rand(n, n);
    x = rand(n, 1);
    solucionExacta = x; %Guardamos la solución exacta para después.
    b = A * x;

    %Listo: Tenemos nuestro sistema de pruebas A x = b. Apliquemos LU:

    U = A;
    L = eye(n, n); %L comienza siendo la matriz identidad.

    t = time(); %Comienzo a contar el tiempo de ejecución

    for k = 1:n - 1

        for i = k + 1:n
            L(i, k) = -U(i, k) / U(k, k);

            for j = 1:n
                U(i, j) = U(i, j) + L(i, k) * U(k, j);
            endfor

            L(i, k) = -L(i, k);
        endfor

    endfor

    %Resolvemos Ly = b para hallar y.
    for k = 1:n
        y(k) = b(k);

        for i = k + 1:n
            b(i) = b(i) - L(i, k) * y(k);
        endfor

    endfor

    %Resolvemos Ux = y para hallar finalmente x.

    for k = n:-1:1
        x(k) = y(k) / U(k, k);

        for i = 1:k - 1
            y(i) = y(i) - U(i, k) * x(k);
        endfor

    endfor

    tiempos = [tiempos; time() - t];
    enes = [enes; n];

    %Imprimimos el tiempo de ejecución y el error cometido para ver si es válido.
    printf('%d, %f %f\n', n, tiempos(end), norm(solucionExacta - x))

endfor
