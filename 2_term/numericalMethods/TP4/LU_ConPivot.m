% @Matias Serino Marin
% Instituto Balseiro - 2022

dimensionProblema = 1;

function result = funcioncita(dimensionProblema)
    n = jota;
    % SEL: A*x = b generado aleatoriamente.
    A = rand(n, n);
    x = rand(n, 1);

    %Reemplazamos el elemento A(1,1) por 10^-10
    A(1, 1) = 10^-20;

    solucionExacta = x;
    b = A * x;
    originalB = b;

    %Listo: Tenemos nuestro sistema de pruebas A x = b. Apliquemos LU SIN PIVOT:
    U = A;
    L = eye(n, n); %L comienza siendo la matriz identidad.

    for k = 1:n - 1

        for i = k + 1:n
            L(i, k) = -U(i, k) / U(k, k);

            for j = 1:n
                U(i, j) = U(i, j) + L(i, k) * U(k, j);
            endfor

            L(i, k) = -L(i, k);
        endfor

    endfor

    %Resolvemos Ly = b para hallar y.
    for k = 1:n
        y(k) = b(k);

        for i = k + 1:n
            b(i) = b(i) - L(i, k) * y(k);
        endfor

    endfor

    %Resolvemos Ux = y para hallar finalmente x.

    for k = n:-1:1
        x(k) = y(k) / U(k, k);

        for i = 1:k - 1
            y(i) = y(i) - U(i, k) * x(k);
        endfor

    endfor

    "Reporte parcial de resultados:"
    solucionSinPivot = x
    solucionExacta

    %-----------------------------------------------------%
    % Ahora solucionémoslo con PIVOT:

    U = A;
    b = originalB; %Guardo el vector b original
    L = eye(n, n); %L comienza siendo la matriz identidad.

    PGLOBAL = eye(n);

    for k = 1:n - 1
        P = eye(n); %Generamos matriz de pivoteo. Comienza siendo identidad.

        indexOfMaxPivot = k;
        % Recorro las filas de esa columna buscando el pivote mayor
        for f = k:n

            if (abs(U(f, k)) > abs(U(indexOfMaxPivot, k)))
                % Hay un pivote más grande!
                indexOfMaxPivot = f;
            endif

        endfor

        if (k != indexOfMaxPivot)
            P(k, indexOfMaxPivot) = 1;
            P(k, k) = 0;
            P(indexOfMaxPivot, k) = 1;
            P(indexOfMaxPivot, indexOfMaxPivot) = 0;
        endif

        U = P * U; % Intercambiamos las filas necesarias de U, L y  b
        b = P * b;
        L = P * L * P; %Permutar L como dice el apunte

        PGLOBAL = P * PGLOBAL;

        for i = k + 1:n
            L(i, k) = -U(i, k) / U(k, k);

            for j = 1:n
                U(i, j) = U(i, j) + L(i, k) * U(k, j);
            endfor

            L(i, k) = -L(i, k);
        endfor

    endfor

    %Resolvemos Ly = b para hallar y.
    for k = 1:n
        y(k) = b(k);

        for i = k + 1:n
            b(i) = b(i) - L(i, k) * y(k);
        endfor

    endfor

    %Resolvemos Ux = y para hallar finalmente x.

    for k = n:-1:1
        x(k) = y(k) / U(k, k);

        for i = 1:k - 1
            y(i) = y(i) - U(i, k) * x(k);
        endfor

    endfor

    "Reporte parcial de resultados:"
    solucionConPivot = x
    solucionExacta

    %Imprimimos la diferencia entre el resultado con y sin PIVOT
    printf('Error de la solución con pivot: %f\n', norm(solucionConPivot - solucionExacta));
    printf('Error de la solución sin pivot: %f\n', norm(solucionSinPivot - solucionExacta));

endfunction

funcioncita(5);
