% @Matias Serino Marin
% Instituto Balseiro - 2022

enes = [];
numerosDeCondicionA = [];
numerosDeCondicionAPreconditioned = [];

for n = 1:1:10

    N = 8 * (n * n * n);

    semilla = zeros(n);
    tolerancia = 10e-6;
    maximumIterations = N;

    SD = sparse(1:N, 1:N, 6 * ones(N, 1), N, N);
    if1 = setdiff(1:N - 1, n:n:n * (8 * n^2 - 1));
    SU1 = sparse(if1, if1 + 1, -1 * ones(1, (n - 1) * 8 * n^2), N, N);
    [i, j] = find([ones(n * (2 * n - 1), 4 * n); zeros(n, 4 * n)]);
    if2 = i + (j - 1) * 2 * n^2;
    SU2 = sparse(if2, if2 + n, -1 * ones(1, n * (2 * n - 1) * 4 * n), N, N);
    if3 = 1:2 * n^2 * (4 * n - 1);
    SU3 = sparse(if3, if3 + 2 * n^2, -1 * ones(1, 2 * n^2 * (4 * n - 1)), N, N);

    A = SD + SU1 + SU1' + SU2 + SU2' + SU3 + SU3'; % Almacenamiento "ralo"
    b = ones(N, 1) / (n + 1)^2;

    % Defino la matriz de preacondicionamiento:
    [L, U] = ilu(A); % Factoriza LU pero aproximadamente: Mantiene estructura RALA de A.

    % Guardo el número de condición de A
    numeroCondicionA = cond(A);

    M = L * U;

    [M_AUTOVECTORS, M_AUTOVALUES] = eig(M);
    M_AUTOVALUES = sqrt(M_AUTOVALUES); %Matriz C diagonal

    %Devuelvo C a su base original
    C = M_AUTOVECTORS * M_AUTOVALUES * inv(M_AUTOVECTORS);
    C_INVERSE = inv(C);

    numeroCondicionAPreconditioned = cond(C_INVERSE * A * C_INVERSE);

    enes = [enes; n];
    numerosDeCondicionA = [numerosDeCondicionA; numeroCondicionA];
    numerosDeCondicionAPreconditioned = [numerosDeCondicionAPreconditioned; numeroCondicionAPreconditioned];

    printf('%d %f %f \n', n, numeroCondicionA, numeroCondicionAPreconditioned);

endfor
